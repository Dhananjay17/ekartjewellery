<!DOCTYPE html>
<html lang="en">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Ecart Products</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/font-awesome.min.css" rel="stylesheet">
<link href="resources/css/prettyPhoto.css" rel="stylesheet">
<link href="resources/css/price-range.css" rel="stylesheet">
<link href="resources/css/animate.css" rel="stylesheet">
<link href="resources/css/main.css" rel="stylesheet">
<link href="resources/css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="resources/images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="resources/images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="resources/images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="resources/images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
	<jsp:include page="/WEB-INF/views/common/navbar.jsp"></jsp:include>
 <br><br>
	<!--/header-->
	<section>
		<div class="container">
			<div class="row">
				<div class="padding-right">
					<div class="features_items">
						<c:choose>
							<c:when test="${empty subCatProds}">
								<c:forEach var="catProds" items="${catProds}">
									<c:url var="url" value="product">
										<c:param name="productId" value="${catProds.productId}" />
									</c:url>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="https://s3.amazonaws.com/srishtibiz/${catProds.productId}.jpg"
														alt="" /> <a href="${url}">
														<h2>
															<c:out value="${catProds.name}" />
														</h2>
													</a>
													<p>
														Rs.
														<c:out value="${catProds.price}" />
													</p>
													<c:url var="add" value="/addProduct">
														<c:param name="productId" value="${catProds.productId}" />
													</c:url>
													<a href="${url}" class="btn btn-default add-to-cart"><i
														class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												<div class="product-overlay">
													<div class="overlay-content">
														<a href="${url}">
															<h2>
																<c:out value="${catProds.name}" />
															</h2>
														</a>
														<p>
															Rs.
															<c:out value="${catProds.price}" />
														</p>
														<c:url var="add" value="/addProduct">
															<c:param name="productId" value="${catProds.productId}" />
														</c:url>
														<a href="${url}" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
													</div>
												</div>
											</div>
											<div class="choose">
												<ul class="nav nav-pills nav-justified">
													<li><a href="#"><i class="fa fa-plus-square"></i>Add
															to wishlist</a></li>
													<li><a href="#"><i class="fa fa-plus-square"></i>Add
															to compare</a></li>
												</ul>
											</div>
										</div>
									</div>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<c:if test="${not empty subCatProds}"></c:if>
								<c:forEach var="subCatProds" items="${subCatProds}">
									<c:url var="url" value="product">
										<c:param name="productId" value="${subCatProds.productId}" />
									</c:url>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img
														src="https://s3.amazonaws.com/srishtibiz/${subCatProds.productId}.jpg"
														alt="" /> <a href="${url}">
														<h2>
															<c:out value="${subCatProds.name}" />
														</h2>
													</a>
													<p>
														Rs.
														<c:out value="${subCatProds.price}" />
													</p>
													<c:url var="add" value="/addProduct">
														<c:param name="productId" value="${subCatProds.productId}" />
													</c:url>
													<a href="${url}" class="btn btn-default add-to-cart"><i
														class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												<div class="product-overlay">
													<div class="overlay-content">
														<a href="${url}">
															<h2>
																<c:out value="${subCatProds.name}" />
															</h2>
														</a>
														<p>
															Rs.
															<c:out value="${subCatProds.price}" />
														</p>
														<c:url var="add" value="/addProduct">
															<c:param name="productId"
																value="${subCatProds.productId}" />
														</c:url>
														<a href="${url}" class="btn btn-default add-to-cart"><i
															class="fa fa-shopping-cart"></i>Add to cart</a>
													</div>
												</div>
											</div>
											<div class="choose">
												<ul class="nav nav-pills nav-justified">
													<li><a href="#"><i class="fa fa-plus-square"></i>Add
															to wishlist</a></li>
													<li><a href="#"><i class="fa fa-plus-square"></i>Add
															to compare</a></li>
												</ul>
											</div>
										</div>
									</div>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</section>

	
	<!--/Footer-->



	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/jquery.scrollUp.min.js"></script>
	<script src="resources/js/price-range.js"></script>
	<script src="resources/js/jquery.prettyPhoto.js"></script>
	<script src="resources/js/main.js"></script>
	<br><br>
	<jsp:include page="/WEB-INF/views/common/footer.jsp"></jsp:include>
</body>
</html>